
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>@yield('title' , 'Dashboard')</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ @asset('css/app.css')}}" rel="stylesheet">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">MD FAJAL</a>
        <ul class="navbar-nav px-3">
          <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('logout') }}"  
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
          </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                      <li class="nav-item">
                          <a class="nav-link @if(route('adminhome'))  @endif" href="{{route('adminhome')}}">
                          <span data-feather="home"></span>
                          Dashboard <span class="sr-only">(current)</span>
                          </a>
                      </li>
                      <li class="nav-item ">
                          <a class="nav-link @if(route('category.index'))  @else '' @endif" href="{{route('category.index')}}">
                          <span data-feather="shopping-cart"></span>
                              Category
                          </a>
                      </li>
                      <li class="nav-item ">
                          <a class="nav-link @if(route('portfolio.index'))  @else '' @endif" href="{{route('portfolio.index')}}">
                          <span data-feather="shopping-cart"></span>
                              All Portfolio
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link @if(route('category.index')) @endif" href="{{route('companylogo.index')}}">
                          <span data-feather="shopping-cart"></span>
                            Logo Brand
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link @if(route('slider.index')) @endif" href="{{route('slider.index')}}">
                          <span data-feather="shopping-cart"></span>
                              Sliders
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link " href="{{route('testimonial.index')}}">
                          <span data-feather="shopping-cart"></span>
                              Testimonials
                          </a>
                      </li>
                    </ul>
                </div>
            </nav>
            @yield('body')
        </div>
    </div>
    <script src="{{@asset('js/app.js')}}"></script>
    </body>
</html>
