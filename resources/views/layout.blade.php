<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <title>@yield('title' , 'MD FAJAL')</title>
        
        <link href="https://fonts.googleapis.com/css?family=Karla:400,400i,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet">
        <link rel="stylesheet" href="css/lightbox.min.css">
        <link rel="stylesheet" href="css/style.min.css">
        <link rel="stylesheet" href="css/custom-8.css">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    </head>
    <body class="preloader-active" >
        <div class="wp-box">
            <a href="https://api.whatsapp.com/send?phone=+91-9096409213">
                <img src="/img/whatsapp.svg"/>
            </a>
        </div>
        <!--== Start Preloader Content ==-->
        <div class="preloader-wrap">
            <div class="preloader">
                {{-- <span class="dot"></span> --}}
                <img src="/img/MF Logo Intro.gif" width="180px" alt="Intro Logo">
                {{-- <div class="dots">
                    <span></span> 
                    <span></span> 
                    <span></span>
                </div> --}}
            </div>
        </div>
        <!--== End Preloader Content ==-->
        <header class="header-area border-bottom sticky-header">
            <div class="container-fluid">
                <div class="row no-gutters align-items-center">
                    <div class="col-5 col-lg-2">
                        <div class="header-logo-area">
                            <a href="{{route('home')}}"><img class="logo-main" width="60px" src="img/logo.svg" alt="MD FAJAL LOGO"> <img class="logo-light" src="img/logo.svg" alt="MD Fajal Logo"></a>
                        </div>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <div class="header-navigation-area">
                            <ul class="main-menu nav justify-content-center">
                                <li><a href="{{route('home')}}">Home</a>
                                <li><a href="{{route('about-me')}}">About Me</a>
                                <li><a href="{{route('hireme')}}">Hire Me</a>
                                <li><a href="{{route('portfolios')}}">Portfolio</a>
                            </ul>
                        </div>
                    </div>
                    <div class="col-7 col-lg-2">
                        <div class="header-action-area text-right">
                            <div class="header-action-area d-none d-lg-block">
                                <a href="https://www.facebook.com/md.fajal.3/" class="btn-cart">
                                    <i class="fa fa-facebook-f"></i> 
                                </a>
                                <a href="https://www.instagram.com/mf_graphicsdesign95/" class="btn-cart">
                                    <i class="fa fa-instagram"></i> 
                                </a>
                                <a href="https://twitter.com/fajal102" class="btn-cart">
                                    <i class="fa fa-twitter"></i> 
                                </a>
                                <a href="https://www.linkedin.com/in/mohammad-fajal-091671190/" class="btn-cart">
                                    <i class="fa fa-linkedin"></i> 
                                </a>
                            </div>
                        <button class="btn-menu d-lg-none">
                            <span></span> 
                            <span></span> 
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <main class="site-wrapper site-wrapper-reveal">
        @yield('body')
        </main>
        <footer class="footer-area reveal-footer footer-area--layout-2 bg-softWhite">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4 text-center mb-sm-10">
                        <div class="copyright-txt mt-sm-10">
                            &copy; All Rights reserved
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <nav class="footer-menu-wrap">
                            <ul class="footer-menu nav justify-content-center header-action-area">
                                <a href="https://www.facebook.com/md.fajal.3/" class="btn-cart">
                                    <i class="fa fa-facebook-f"></i> 
                                </a>
                                <a href="https://www.instagram.com/mf_graphicsdesign95/" class="btn-cart">
                                    <i class="fa fa-instagram"></i> 
                                </a>
                                <a href="https://twitter.com/fajal102" class="btn-cart">
                                    <i class="fa fa-twitter"></i> 
                                </a>
                                <a href="https://www.linkedin.com/in/mohammad-fajal-091671190/" class="btn-cart">
                                    <i class="fa fa-linkedin"></i> 
                                </a>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4 text-center text-md-right">
                        <div class="copyright-txt mt-sm-10">
                            <p> 
                                <a class="link" href="{{route('home')}}">Md Fajal</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <aside class="off-canvas-wrapper">
            <div class="off-canvas-inner">
                <!-- Start Off Canvas Content Wrapper -->
                <div class="off-canvas-content">
                    <!-- Off Canvas Header -->
                    <div class="off-canvas-header">
                        <div class="logo-area">
                            <a href="{{route('home')}}"><img src="img/logo.svg" width="60px" alt="MD FAJAL LOGO"></a>
                        </div>
                        <div class="close-action">
                            <button class="btn-close"><i class="pe-7s-close"></i></button>
                        </div>
                    </div>
                    <div class="off-canvas-item">
                        <!-- Start Mobile Menu Wrapper -->
                        <div class="res-mobile-menu">
                            <!-- Note Content Auto Generate By Jquery From Main Menu -->
                        </div>
                        <!-- End Mobile Menu Wrapper -->
                    </div>
                </div>
                <!-- End Off Canvas Content Wrapper -->
            </div>
        </aside>
        <script src="js/main.min2.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/custom.js"></script>
        <script>
            lightbox.option({
                'resizeDuration': 200,
                'wrapAround': true,
                'alwaysShowNavOnTouchDevices': true,
            })
        </script>
    </body>
</html>
