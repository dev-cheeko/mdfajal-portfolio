@component('mail::message')
    #Contact Form Submission <br>
    Name : {{$details['name']}}<br>
    Email Address : {{$details['email']}}<br>
    Contact : {{$details['contact']}}<br>
    Category : {{$details['category']}}<br>
    State : {{$details['state']}}<br>
    Country : {{$details['country']}}<br>
    Message : {{$details['message']}}<br>
@endcomponent