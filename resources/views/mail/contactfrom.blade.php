@component('mail::message')
    #Contact Form Submission <br>
    Name : {{$details['name']}}<br>
    Email Address : {{$details['email']}}<br>
    Contact : {{$details['contact']}}<br>
    Message : {{$details['message']}}<br>
@endcomponent