@extends('layout')
@section('title') Portfolio @endsection
@section('body') 
  <div class="portfolio-page-content-wrapper sp-y">
    <div class="">
        <div class="portfolio-content-header">
            <div class="row">
                <div class="col-lg-6 text-center m-auto">
                    <div class="section-title">
                        <h2>PORTFOLIO</h2>
                        <p>See some of my best works for my clients. </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="portfolio-content-body">
            <div class="row">
                <div class="col-12">
                    <div class="filter-menu-wrap mb-80 mb-md-60 mb-sm-40">
                        <ul class="filter-menu nav justify-content-center">
                            <li class="active ht-tooltip"  data-tippy-content="{{ (!$portfolios->isEmpty()) ? count($portfolios) : '0'}}  " data-filter="*" tabindex="0">All</li>
                            <li class="ht-tooltip"  data-filter=".broucher" data-tippy-content="
                            @if(!$categories->isEmpty())
                            
                            @endif
                            " tabindex="0">Broucher</li>
                            <li class="ht-tooltip"  data-filter=".social-post" tabindex="0">Social Media Post</li>
                        </ul>
                    </div>
                    <div class="portfolio-content-wrap">
                        <div class="row mtn-30 no-gutters filter-content">
                            @if(!$portfolios->isEmpty())
                             @foreach ($portfolios as $portfolio)      
                                <div class="col-sm-6 col-lg-3 {{$portfolio->categories->slug}}">
                                    <a href="/uploads/{{$portfolio->photo}}" title="{{ucfirst($portfolio->title)}}" data-lightbox="{{ucfirst($portfolio->categories->name)}}" data-title="{{ucfirst($portfolio->title)}}">
                                        <div class="portfolio-item">
                                            <div class="portfolio-item__thumb">
                                                <img src="/uploads/{{$portfolio->photo}}" alt="{{ucfirst($portfolio->title)}}">
                                            </div>
                                            <div class="portfolio-item__info">
                                                <h3 class="title text-white">{{ucfirst($portfolio->title)}}</h3>
                                                <h6 class="category">{{ucfirst($portfolio->categories->name)}}</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                             @endforeach
                            @else
                            <div class="text-center">
                                <p>No Result Found</p>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="d-flex justify-content-center mt-5">
                        {{ $portfolios->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection