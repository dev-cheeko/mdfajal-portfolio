@extends('layout')
@section('title') Hire Me @endsection
@section('body') 
<div class="contact-page-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d449.7122462779931!2d85.15851060296082!3d25.614950666460214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sD.N.%20Das%20Lane%2C%20Langertoli%2C%20opposite%20Ajay%20Bhawan%2C%20Patna-4!5e0!3m2!1sen!2sin!4v1587739130942!5m2!1sen!2sin" width="100%" height="290" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
<div class="contact-page-contact-info sp-y">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title mb-0">
                    <h2>Hire Me</h2></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="call-to-contact">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>ADDRESS</h5>
                            <h6 class="mb-0">D.N. Das Lane, Langertoli, opposite Ajay Bhawan, Patna-4</h6>
                        </div>
                        <div class="col-md-6 mt-sm-25">
                            <h5>MESSAGE ME AT</h5>
                            <h6 class="mb-0">info@mfgraphics.in</h6>
                            <h6 class="mb-0">+91-9096409213</h6>
                        </div>
                    </div>
                </div>
                <div class="contact-form-wrap sm-top-wp">
                    <form id="ctform"  method="post">
                        @csrf
                        <div class="contact-from-content mtn-70 mtn-sm-30">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-input-item">
                                        <input type="text" name="con_name" placeholder="Name" required id="ctfullname">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-input-item">
                                        <input type="email" name="con_email" placeholder="Email" required id="ctemail">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-input-item">
                                        <input type="text" name="con_phone" placeholder="Phone Number" id="ctcontact">
                                    </div>
                                </div>
                            </div>
                            <div class="row align-items-end">
                                <div class="col-lg-10">
                                    <div class="form-input-item">
                                        <label for="message" class="sr-only">Message</label>
                                        <textarea name="con_message" id="ctmsg" cols="30" rows="2" placeholder="Your Message" required></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-input-item">
                                        <button class="btn btn-bordered ctbtn-s">Send Messages</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Message Notification -->
                        <div id="submMsg" class="mt-3"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection