@extends('layout')
@section('title') About Me @endsection
@section('body')
<div class="about-page-content-area px-lg-5">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-6 order-1 order-lg-0">
                <div class="about-page-content">
                    <h3 class="text-uppercase hight">Hi, I am MD Fajal <span class="blinkcursor">|</span></h3>
                    <p>A freelance Graphic Designer from Bhagalpur, Bihar. I started my career as a Graphic Designer in Aug, 2014 when i was in Delhi, pursuing my degree in Graphic Design. Later, I started working as a full time employee with the Aptara Pvt. Ltd. Company in Noida, Delhi. 
                    After working 5 months there I returned to Bhagalpur and started my freelance career and now i m a fulltime freelance Graphic Designer with the wide knowledge of Graphic Design. 
                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-0 ml-auto">
                <figure class="about-thumb about-video-wrap position-relative"><img src="/img/profile-picture.jpg" class="w-100" alt="MD FAJAL">
                </figure>
            </div>
        </div>
    </div>
</div>
<div class="about-personal-skill-area sm-top">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-6">
                <div class="personal-skill-left bg-img" data-bg="">
                    <div class="section-title mb-0">
                        <h2 class="mt-0">Personal skills</h2>
                        <p>I have been constantly developing and empowering my skills to work anywhere and whoever with. See some of my personal skills.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="personal-skill-right bg-softWhite">
                    <div class="skill-wrapper mtn-45 w-100">
                        <!-- Start Skills Item #1 -->
                        <div class="progress-bar-item">
                            <div class="progress-info"><span class="progress-info__title">Graphic Softwares Skills</span> <span class="progress-info__percent percent"></span></div>
                            <div class="progress-line">
                                <div class="progress-line-bar" data-percent="85%"></div>
                            </div>
                        </div>
                        <!-- Start Skills Item #2 -->
                        <div class="progress-bar-item">
                            <div class="progress-info"><span class="progress-info__title">IDEAS</span> <span class="progress-info__percent percent"></span></div>
                            <div class="progress-line">
                                <div class="progress-line-bar" data-percent="80%"></div>
                            </div>
                        </div>
                        <!-- Start Skills Item #3 -->
                        <div class="progress-bar-item">
                            <div class="progress-info"><span class="progress-info__title">Creativity</span> <span class="progress-info__percent percent"></span></div>
                            <div class="progress-line">
                                <div class="progress-line-bar" data-percent="75%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="call-to-action-modern bg-brand">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="call-to-action-content call-to-action-content--business">
                    <h4>Download My Resume.</h4><a href="/img/resume (Final) For Websiet.pdf" download="/img/resume (Final) For Websiet.pdf" class="btn btn-bordered">My Resume</a></div>
            </div>
        </div>
    </div>
</div>

@endsection