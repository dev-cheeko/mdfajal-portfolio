@extends('layout')
@section('body')
<div class="hero-slider-area">
    <div id="rev_slider_14_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic-shop" data-source="gallery">
        <div id="rev_slider_14_1" class="rev_slider fullwidthabanner" data-version="5.4.7">
            <ul>
                @if(!$sliders->isEmpty())
                    @foreach ($sliders as $slider)         
                    <li data-index="rs-36" data-transition="3dcurtain-horizontal,3dcurtain-vertical,cube-horizontal,turnoff,parallaxtoright,parallaxtotop,parallaxhorizontal,slotfade-horizontal,slidingoverlayleft" data-slotamount="default,default,default,default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-randomtransition="on" data-easein="default,default,default,default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default,default,default,default,default" data-thumb="/uploads/sliders/{{$slider->img}}" data-rotate="0,0,0,0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide">
                    <img src="/uploads/sliders/{{$slider->img}}" alt="{{ucfirst($slider->title)}}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    </li>
                    @endforeach
                @endif
            </ul>
            <div class="tp-bannertimer tp-bottom"></div>
        </div>
    </div>
</div>
<div class="portfolio-wrapper sm-top">
    <div class="">
        <div class="portfolio-content-wrap">
            <div class="row">
                <div class="col-lg-8 m-auto text-center">
                    <div class="section-title">
                        <h5>Recent Works</h5>
                        <h2 class="text-uppercase">Portfolio</h2>
                    </div>
                </div>
            </div>
            <div class="row mtn-30 masonryGrid no-gutters">
                @if(!$portfolios->isEmpty())
                    @foreach ($portfolios as $portfolio)
                    <div class="col-sm-6 col-lg-3">
                        <a href="/uploads/{{$portfolio->photo}}" title="{{ucfirst($portfolio->title)}}" data-lightbox="{{ucfirst($portfolio->categories->name)}}" data-title="{{ucfirst($portfolio->title)}}">
                            <div class="portfolio-item">
                                <div class="portfolio-item__thumb">
                                    <img src="/uploads/{{$portfolio->photo}}" alt="{{ucfirst($portfolio->title)}}">
                                </div>
                                <div class="portfolio-item__info">
                                    <h3 class="title text-white">{{ucfirst($portfolio->title)}}</h3>
                                    <h6 class="category">{{ucfirst($portfolio->categories->name)}}</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                @else
                <div class="d-block text-center">
                    <p class="">No Result Found</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="about-personal-skill-area sm-top">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-6">
                <div class="personal-skill-left bg-img" data-bg="">
                    <div class="section-title mb-0">
                        <h2 class="mt-0 text-upp">Personal Skills</h2>
                        <p>I have been constantly developing and empowering my skills to work anywhere and whoever with. See some of my personal skills.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="personal-skill-right bg-softWhite">
                    <div class="skill-wrapper mtn-45 w-100">
                        <!-- Start Skills Item #1 -->
                        <div class="progress-bar-item">
                            <div class="progress-info"><span class="progress-info__title">Graphic Softwares Skills</span> <span class="progress-info__percent percent"></span></div>
                            <div class="progress-line">
                                <div class="progress-line-bar" data-percent="85%"></div>
                            </div>
                        </div>
                        <!-- Start Skills Item #2 -->
                        <div class="progress-bar-item">
                            <div class="progress-info"><span class="progress-info__title">IDEAS</span> <span class="progress-info__percent percent"></span></div>
                            <div class="progress-line">
                                <div class="progress-line-bar" data-percent="80%"></div>
                            </div>
                        </div>
                        <!-- Start Skills Item #3 -->
                        <div class="progress-bar-item">
                            <div class="progress-info"><span class="progress-info__title">Creativity</span> <span class="progress-info__percent percent"></span></div>
                            <div class="progress-line">
                                <div class="progress-line-bar" data-percent="75%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if(!$testimonials->isEmpty())
<div class="testimonial-area">
    <div class="testimonial-header-area bg-softWhite">
       <div class="container">
          <div class="row">
             <div class="col-12 text-center">
                <div class="section-title">
                   <h5>Our Clients</h5>
                   <h4 class="text-uppercase">What they say about me</h4>
                </div>
             </div>
          </div>
       </div>
    </div>
<div class="testimonial-content-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
            <!-- Testimonial Carousel Content -->
            <div class="testimonial-content">
                <!-- Start Single Testimonial Item -->
                @foreach($testimonials as $testimonial)
                <div class="testimonial-item">
                    <figure class="client-thumb"><img src="{{$testimonial->image}}" alt="{{ucfirst($testimonial->title)}}"></figure>
                    <p>{{ucfirst($testimonial->testimonialdetail)}}</p>
                    <span class="quote"><i class="fa fa-quote-left" style="color: #af1b1b;"></i></span>
                    <h6 class="client-title">{{ucfirst($testimonial->title)}}</h6>
                    <span class="client-designation">{{ucfirst($testimonial->designation)}}</span>
                </div>
                @endforeach
            </div>
        </div>
     </div>
  </div>
</div>
</div>
@endif
<section>
    <div class="brand-logo-area">
        <div class="container">
            <div class="row">
            <div class="col-lg-8 m-auto text-center">
                <div class="section-title">
                    <h4 class="text-uppercase">My work with Companies</h4>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="brand-logo-content">
                        @if(!$companies->isEmpty())
                            @foreach ($companies as $company)
                            <div class="brand-logo-item">
                                <a href="/uploads/logos/{{$company->company_logo}}" data-lightbox="{{$company->company_logo}}" data-title="{{ucfirst($company->title)}}"> 
                                    <img src="/uploads/logos/{{$company->company_logo}}" alt="{{ucfirst($company->title)}}">
                                </a>
                            </div>
                            @endforeach
                        @else
                            <p class="text-center">No Result Found</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-logo-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 m-auto text-center">
                    <div class="section-title">
                        <h4 class="text-uppercase">I've worked for Brands</h4></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="brand-logo-content">
                        @if(!$brands->isEmpty())
                            @foreach ($brands as $brand)
                            <div class="brand-logo-item">
                                <a href="/uploads/logos/{{$brand->company_logo}}" data-lightbox="{{$brand->company_logo}}" data-title="{{ucfirst($brand->title)}}" >
                                    <img src="/uploads/logos/{{$brand->company_logo}}" class="img-fluid" alt="{{ucfirst($brand->title)}}">
                                </a>
                            </div>
                            @endforeach
                        @else
                        <div class="d-block text-center">
                            <p class="">No Result Found</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection