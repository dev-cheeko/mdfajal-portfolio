@extends('layout-admin')
@section('title') Add Testimonial @endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 m-auto">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <ol class="breadcrumb bg-white py-0 pl-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{route('adminhome')}}">Home</a></li>
                                    <li class="breadcrumb-item active">Add testimonial</li>
                                </ol>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{route('testimonial.index')}}" class="btn btn-primary btn-sm " id="addrolebtn">Testimonial list</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('testimonial.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" placeholder="Name" name="name"  value="{{old('name')}}" id="rolename" class="form-control @error('name') is-invalid @enderror">
                                @error('name') 
                                    <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="designation">Designation</label>
                                <input type="text" name="designation"  value="{{old('designation')}}" id="designation" class="form-control @error('designation') is-invalid @enderror">
                                @error('designation') 
                                    <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="desc">Description</label>
                                <textarea type="text" name="desc" row="5" col="30" value="{{old('desc')}}" id="desc" class="form-control @error('desc') is-invalid @enderror"></textarea>
                                @error('desc') 
                                    <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" name="image" id="image">
                                @error('desc') 
                                <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-outline-success btn-block w-100">Add testimonial</button>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</main>
@endsection