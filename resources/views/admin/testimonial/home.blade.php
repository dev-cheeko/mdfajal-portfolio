@extends('layout-admin')
@section('title') Testimonials @endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-3">
    <div class="container-fluid">
        <div class="card ">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb bg-white py-0 pl-0 mb-0">
                            <li class="breadcrumb-item"><a href="{{route('adminhome')}}">Home</a></li>
                            <li class="breadcrumb-item active">Testimonials</li>
                        </ol>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{route('testimonial.create')}}" class="btn btn-primary btn-sm " id="">Add testimonial</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if($fs = Session::get('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$fs}}
                    </div>
                @endif
                @if($fs = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$fs}}
                    </div>
                @endif
                <table class="table table-bordered ">
                    <thead class="">
                      <tr class="text-small small">
                        <th>#</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Detail</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($testimonials as $testimonial)    
                      <tr>
                        <td>{{$testimonial->id}}</td>
                        <td>
                            
                            <img src="{{$testimonial->image}}" class="img-fluid img-thumbnail" width="100px" alt="">
                        </td>
                        <td>{{ ucfirst($testimonial->title) }}</td>
                        <td>{{ ucfirst($testimonial->designation) }}</td>
                        <td>{{$testimonial->testimonialdetail}}</td>
                        <td>{{$testimonial->created_at->diffForHumans()}}</td>
                        <td>{{$testimonial->updated_at->diffForHumans()}}</td>
                        <td>
                         <div class="btn-group-sm">
                           <a href="{{route('testimonial.edit' , $testimonial->id)}}" role="button" class="btn btn-sm btn-info d-inline">Edit</a>
                           <form class="d-inline" onSubmit="return confirm('Are you sure you wish to delete?');" method="post" action="{{route('testimonial.destroy' , $testimonial->id)}}">
                               @method('DELETE')
                               @csrf
                               <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                           </form>
                       </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</main>
@endsection