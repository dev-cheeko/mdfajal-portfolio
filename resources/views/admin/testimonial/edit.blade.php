@extends('layout-admin')
@section('title') Edit Testimonial @endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 m-auto">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <ol class="breadcrumb bg-white py-0 pl-0 mb-0">
                                    <li class="breadcrumb-item"><a href="{{route('adminhome')}}">Home</a></li>
                                    <li class="breadcrumb-item active">Edit testimonial</li>
                                </ol>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{route('testimonial.create')}}" class="btn btn-primary btn-sm " id="addrolebtn">Add testimonial</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('testimonial.update' , $testimonial->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" placeholder="Name" name="name"  value="{{$testimonial->title}}" id="rolename" class="form-control @error('name') is-invalid @enderror">
                                @error('name') 
                                    <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="designation">Designation</label>
                                <input type="text" name="designation"  value="{{$testimonial->designation}}" id="designation" class="form-control @error('designation') is-invalid @enderror">
                                @error('designation') 
                                    <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="desc">Description</label>
                                <textarea type="text" name="desc" row="5" col="30" id="desc" class="form-control @error('desc') is-invalid @enderror">{{$testimonial->testimonialdetail}}</textarea>
                                @error('desc') 
                                    <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" name="image" id="image">
                                @error('desc') 
                                <small class="text-danger d-block">{{ $message }}</small>
                                @enderror
                                <a onclick="return false;" href="{{asset('/storage/'.$testimonial->image)}}" data-lightbox="{{asset('/storage/'.$testimonial->image)}}">
                                    <img class="img-fluid" width="100px" src="{{asset('/storage/'.$testimonial->image)}}" alt="">
                                </a>
                            </div>
                            <button type="submit" class="btn btn-outline-success btn-block w-100">Update testimonial</button>
                        </form>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</main>
@endsection