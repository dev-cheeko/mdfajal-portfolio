@extends('layout-admin')
@section('title')
Add Logo
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Add Logo</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('companylogo.index')}}">Logo List</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 m-auto">
            @if ($fs = Session::get('uploadErr'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Add Logo</div>
                <div class="card-body">
                    <form action="{{route('companylogo.store')}}" method="post" enctype="multipart/form-data" >
                        @csrf
                        <div class="form-group">
                            <label for="title">Title*</label>
                            <input type="text" name="title" placeholder="Title" 
                            class="form-control" id="title" value="">
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">Select Types</option>
                                <option value="brand">Brand</option>
                                <option value="company">Company</option>
                            </select>
                        </div>
                       <div class="form-group">
                        <label for="logoimg">Logo Image</label>
                        <input type="file" name="logoimg" class="" id="logoimg">
                        <p class="small">Upload Jpeg, Jpg, Png only less than 2 mb </p>
                       </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection