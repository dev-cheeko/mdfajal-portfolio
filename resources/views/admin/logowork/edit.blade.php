@extends('layout-admin')
@section('title')
Edit Logo
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Logo</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('companylogo.index')}}">Logo List</a>
            <a class="btn btn-outline-info" href="{{route('companylogo.create')}}">Add Logo</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 m-auto">
            @if ($fs = Session::get('uploadErr'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Add Logo</div>
                <div class="card-body">
                    <form action="{{route('companylogo.update' , $logo->id)}}" method="post" enctype="multipart/form-data" >
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Title*</label>
                            <input type="text" name="title" placeholder="Title" 
                            class="form-control" id="title" value="{{$logo->title}}">
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">Select Types</option>
                                <option value="brand" @if($logo->category == 'brand' ) selected @endif>Brand</option>
                                <option @if($logo->category == 'company' ) selected @endif value="company">Company</option>
                            </select>
                        </div>
                       <div class="form-group">
                        <label for="logoimg">Logo Image</label>
                        <input type="file" name="logoimg" class="" id="logoimg">
                        <p class="small">Upload Jpeg, Jpg, Png only less than 2 mb </p>
                        <img src="/uploads/logos/{{$logo->company_logo}}" class="img-fluid img-thumbnail" width="100px"  alt="">
                       </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection