@extends('layout-admin')
@section('title')
Brand / Company Logo
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Brand / Company Logo</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('companylogo.create')}}">Add Brand / Company Logo</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($fs = Session::get('success'))
                <div class="alert alert-success">
                    {{ $fs}}
                </div>
            @endif
            @if ($fs = Session::get('errdel'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Brand / Comapny Logo 
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Logo Image</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!$logos->isEmpty())
                                @foreach ($logos as $logo)    
                                <tr>
                                    <td>{{$logo->id}}</td>
                                    <td>
                                        <img src="/uploads/logos/{{$logo->company_logo}}" alt="{{ucfirst($logo->title)}}" class="img-fluid img-thumbnail" width="80px">
                                    </td>
                                    <td>{{ucfirst($logo->title)}}</td>
                                    <td>
                                        @if($logo->category == 'brand')
                                        <button class="btn btn-sm btn-success">Brand</button>
                                        @endif
                                        @if($logo->category == 'company')
                                        <button class="btn btn-sm btn-success">Company</button>
                                        @endif
                                    </td>
                                    <td>{{$logo->created_at}}</td>
                                    <td>{{$logo->updated_at}}</td>
                                    <td>
                                        <div class="btn-group-sm">
                                            <a href="{{route('companylogo.edit' , $logo->id)}}" role="button" class="btn btn-sm btn-info d-inline">Edit</a>
                                            <form class="d-inline" method="post" action="{{route('companylogo.destroy' , $logo->id)}}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-danger">Del</button>
                                            </form>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach   
                            @else
                                <tr><td colspan="12" class="text-center">
                                    <p>No Result Found</p>
                                    <a class="btn btn-outline-info" href="{{route('companylogo.create')}}">Add Logo</a>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection