@extends('layout-admin')
@section('title')
Dashboard
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Portfolio</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('portfolio.index')}}">Portfolio List</a>
            <a class="btn btn-outline-info" href="{{route('portfolio.create')}}">Add Portfolio</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 m-auto">
            @if ($fs = Session::get('uploadErr'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Add Porfolio</div>
                <div class="card-body">
                    <form action="{{route('portfolio.update' , $portfolio->id)}}" method="post" enctype="multipart/form-data" >
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="title">Title*</label>
                            <input type="text" name="title" placeholder="Title" 
                            class="form-control" id="title" value="{{$portfolio->title}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" placeholder="Description" 
                            class="form-control" id="description" cols="30" rows="5">{{$portfolio->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category" id="category"   class="form-control">
                                <option value="" >Select Category</option>
                                @if(!$categories->isEmpty())
                                    @foreach ($categories->toArray() as $category)
                                        <option value="{{$category['id']}}" @if(in_array($portfolio->categories->name , $category)) selected @endif >{{$category['name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                       <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="featureimg">FeatureImg</label>
                                    <input type="file" name="featureimg" class="" id="featureimg">
                                    <p class="small">Upload Jpeg, Jpg, Png, Gif and .mp4 only less than 2 mb </p>
                                    <img src="/uploads/{{$portfolio->photo}}" alt="" class="img-fluid img-thumbnail" width="120px">
                                </div>
                                <div class="col-md-6">
                                    <label for="extraimg">Extra Images</label>
                                    <input type="file" name="extraimages[]" class="" id="extraimg">
                                    <p class="small">Upload Jpeg, Jpg, Png only less than 2 mb </p>
                                </div>
                            </div>
                       </div>
                       <div class="form-group">
                            <label for="highlight">Highlight</label>
                            @if($portfolio->highligt == '0') 
                            <input type="checkbox" name="highlight"  class="checkbox-light" >
                            @endif
                            @if($portfolio->highligt == '1') 
                            <input type="checkbox" name="highlight" checked  class="checkbox-light" >
                            @endif
                       </div>
                       
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection