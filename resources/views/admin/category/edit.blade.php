@extends('layout-admin')
@section('title')
Edit Category
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Category</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('category.index')}}">Category List</a>
            <a class="btn btn-outline-info" href="{{route('category.create')}}">Add Category</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 m-auto">
           
            @if ($fs = Session::get('exists'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($fs = Session::get('wrong'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Edit Category</div>
                <div class="card-body">
                    <form action="{{route('category.update' , $editData->id)}}" method="POST" >
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="categoryname">Category Name*</label>
                            <input type="text"  name="name" placeholder="Category Name" 
                            class="form-control" 
                            id="categoryname" value="@if(isset($editData)){{$editData->name}}@endif">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection