@extends('layout-admin')
@section('title')
Category
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Category</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('category.create')}}">Add Category</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Category Lists
                </div>
                <div class="card-body">
                    @if ($fs = Session::get('success'))
                        <div class="alert alert-success">
                            {{ $fs}}
                        </div>
                    @endif
                    @if ($fs = Session::get('categorydeleted'))
                        <div class="alert alert-success">
                            {{ $fs}}
                        </div>
                    @endif
                    @if ($categories->isEmpty())
                        <div>
                            <a class="btn btn-outline-info" href="{{route('category.create')}}">Add Category</a>
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)    
                                    <tr>
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->created_at}}</td>
                                        <td>{{$category->updated_at}}</td>
                                        <td>
                                            <div class="btn-group-sm d-inline">
                                                <a href="{{route('category.edit' , $category->id)}}" role="button" class="btn btn-sm btn-info">Edit</a>
                                                <form method="post" class="d-inline" action="{{route('category.destroy' , $category->id)}}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection