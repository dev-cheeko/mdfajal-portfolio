@extends('layout-admin')
@section('title')
Dashboard
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Add Slider</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('slider.index')}}">Slider Lists</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 m-auto">
            @if ($fs = Session::get('uploadErr'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Add Slider</div>
                <div class="card-body">
                    <form action="{{route('slider.store')}}" method="post" enctype="multipart/form-data" >
                        @csrf
                        <div class="form-group">
                            <label for="title">Title*</label>
                            <input type="text" name="title" placeholder="Title" 
                            class="form-control" id="title" value="{{old('title')}}">
                        </div>
                       <div class="form-group">
                            <label for="img">Slide Img</label>
                            <input type="file" name="img" class="" id="img">
                            <p class="small">Upload Jpeg, Jpg, Png, Gif only less than 8 mb </p>
                       </div>
                       <div class="form-group">
                           <label for="sequence">Sequence</label>
                           <select name="sequence" id="sequence" class="form-control">
                               <option value="">Select Slider Position</option>
                               <option value="1">First</option>
                               <option value="2">Second</option>
                               <option value="3">Third</option>
                               <option value="4">Fourth</option>
                               <option value="5">Fifth</option>
                           </select>
                       </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection