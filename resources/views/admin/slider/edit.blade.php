@extends('layout-admin')
@section('title')
Dashboard
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Edit Slider</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('slider.create')}}">Add Slider</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 m-auto">
            @if ($fs = Session::get('uploadErr'))
                <div class="alert alert-danger">
                    {{ $fs}}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Edit Slider</div>
                <div class="card-body">
                    <form action="{{route('slider.update' , $slider->id)}}" method="post" enctype="multipart/form-data" >
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Title*</label>
                            <input type="text" name="title" placeholder="Title" 
                            class="form-control" id="title" value="{{$slider->title}}">
                        </div>
                       <div class="form-group">
                            <label for="img">Slide Img</label>
                            <input type="file" name="img" class="" id="img">
                            <p class="small">Upload Jpeg, Jpg, Png, Gif only less than 8 mb </p>
                            <img width="100px" class="img-fluid img-thumbnail thumbnail" src="/uploads/sliders/{{$slider->img}}" alt="{{ucfirst($slider->title)}}">
                       </div>
                       <div class="form-group">
                        <label for="sequence">Sequence</label>
                        <select name="sequence" id="sequence" class="form-control">
                            <option value="1" {{ ($slider->sequence  == 1)? 'selected' : '' }} >First</option>
                            <option value="2" {{ ($slider->sequence  == 2)? 'selected' : '' }} >Second</option>
                            <option value="3" {{ ($slider->sequence  == 3)? 'selected' : '' }} >Third</option>
                            <option value="4" {{ ($slider->sequence  == 4)? 'selected' : '' }} >Fourth</option>
                            <option value="5" {{ ($slider->sequence  == 5)? 'selected' : '' }} >Fifth</option>
                        </select>
                    </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection