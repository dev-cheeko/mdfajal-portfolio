@extends('layout-admin')
@section('title')
Dashboard
@endsection
@section('body')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Slider</h1>
        <div>
            <a class="btn btn-outline-info" href="{{route('slider.create')}}">Add Slider</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($fs = Session::get('success'))
                <div class="alert alert-success">
                    {{ $fs}}
                </div>
            @endif
            @if ($fs = Session::get('errdel'))
            <div class="alert alert-danger">
                {{ $fs}}
            </div>
        @endif
            <div class="card">
                <div class="card-header">
                    Slider Lists
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Sequence</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!$sliders->isEmpty())
                                @foreach ($sliders as $slider)    
                                <tr>
                                    <td>{{$slider->id}}</td>
                                    <td>
                                        <img src="/uploads/sliders/{{$slider->img}}" alt="{{ucfirst($slider->title)}}" class="img-fluid img-thumbnail" width="80px">
                                    </td>
                                    <td>{{ucfirst($slider->title)}}</td>
                                    <td>{{$slider->sequence}}</td>
                                    <td>{{$slider->created_at}}</td>
                                    <td>{{$slider->updated_at}}</td>
                                    <td>
                                        <div class="btn-group-sm">
                                            <a href="{{route('slider.edit' , $slider->id)}}" role="button" class="btn btn-sm btn-info d-inline">Edit</a>
                                            <form class="d-inline" method="post" action="{{route('slider.destroy' , $slider->id)}}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-danger">Del</button>
                                            </form>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach   
                            @else
                                <tr><td colspan="12" class="text-center">
                                    <p>No Result Found</p>
                                    <a class="btn btn-outline-info" href="{{route('slider.create')}}">Add Slider</a>
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection