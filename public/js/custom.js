
 // Contact Send Mail
 $("#ctform").on("submit" , function(e){
    e.preventDefault();
    
    var fullname = $("#ctfullname").val();
    var email    = $("#ctemail").val();
    var contact  = $("#ctcontact").val();
    var msg      = $("#ctmsg").val();

    // console.log(firstname);
    $.ajax({
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'X-Requested-With' : 'XMLHttpRequest',
        },
        url: "/contactform",
        data: {
            fullname: fullname,
            email: email,
            msg: msg,
            contact: contact,

        }, 
        // processData: false,
        // contentType: false,
        // cache: false,
        dataType: "JSON",
        beforeSend: function() {
            $(".ctbtn-s").attr('disabled' , true);
            $("#submMsg").html(
                `<div class="alert alert-success d-block ">
                Please wait...
                </div>`
            );
        },
        success: function (response) {
            $("#submMsg").html(
                `<div class="alert alert-success d-block ">${response.msg}</div>`
            );
             // $("#ctform").reset();
             $('#ctform input[type="text"]').val('');
             $('#msg').val('');
             $(".ctbtn-s").attr('disabled' , false);
        },
        statusCode: {
            404: function() {
                $("#submMsg").html(
                    `<div class="alert alert-danger d-block ">All fields are required.</div>`
                );
                $(".ctbtn-s").attr('disabled' , false);
            },
        
            400: function(response) {
                $("#submMsg").html(
                    `<div class="alert alert-danger d-block ">Something went wrong.</div>`
                );
                $(".ctbtn-s").attr('disabled' , false);
           },
           
            422: function(response) {
                $("#submMsg").html(
                    `<div class="alert alert-danger d-block ">Firstname, Email, Message fields are required</div>`
                );
                $(".ctbtn-s").attr('disabled' , false);
           }
        }
    });
});