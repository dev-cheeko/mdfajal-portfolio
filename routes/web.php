<?php

use Illuminate\Support\Facades\Route;

// Pages Controller
Route::get('/', 'PageController@index')->name('home');
Route::get('/aboutme', 'PageController@aboutMe')->name('about-me');
Route::get('/hireme', 'PageController@hireMe')->name('hireme');
Route::get('/portfolio', 'PageController@portfolios')->name('portfolios');

// Admin Routes
Route::prefix('admin')->middleware('auth')->group(function() {
    Route::view('/' , 'admin.home')->name('adminhome');
    Route::resource('portfolio' , 'PortfolioController');
    Route::resource('category' , 'CategoryController');
    Route::resource('companylogo' , 'CompanyLogoController');
    Route::resource('slider' , 'SliderController');
    Route::resource('testimonial', 'TestimonialController');
});
Route::post('/contactform', 'PageController@ctForm')->name('ctForm');
Auth::routes();
