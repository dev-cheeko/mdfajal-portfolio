<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable = ['title' , 'description' , 'photo' , 'slug' , 'category_id' , 'highligt'];

    public function categories() {
        return $this->belongsTo('App\Category' , 'category_id' , 'id');
    }
}
