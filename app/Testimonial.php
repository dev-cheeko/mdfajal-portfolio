<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Testimonial extends Model
{
    protected $guarded = [];


    // Setting testimonial name to lower case
    public function setTitleAttribute($val) {
        $this->attributes['title'] = strtolower($val);
    }


    // Setting slug
    public function setSlugAttribute($val) {
        $this->attributes['slug'] = Str::slug($val , '-');
    }


    // Setting designation
    public function setDesignationAttribute($val) {
        $this->attributes['designation'] = Str::slug($val , '-');
    }


}
