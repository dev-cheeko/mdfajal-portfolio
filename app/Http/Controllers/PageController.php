<?php

namespace App\Http\Controllers;
use App\Portfolio;
use App\CompanyLogo;
use App\Slider;
use App\Category;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactFormMail;
class PageController extends BaseController
{
    public function index() {
        $sliders = Slider::orderBy('sequence' , 'ASC')->limit(5)->get(); 
        $testimonials = Testimonial::orderBy('id' , 'DESC')->get();
        $Companieslogos = CompanyLogo::where('category' , '=' ,'company')->orderBy('id' , 'DESC')->limit(10)->get();
        $brandslogos = CompanyLogo::where('category' , '=' ,'brand')->orderBy('id' , 'DESC')->limit(10)->get();
        $portfolios = Portfolio::with('categories')
            ->where('highligt' , '=' , '1' )
            ->orderBy('id' , 'DESC')
            ->limit(12)
            ->get();

        
        return view('pages.home' , [
            'brands'    => $brandslogos,
            'companies' => $Companieslogos,
            'portfolios' => $portfolios,
            'sliders' => $sliders,
            'testimonials' => $testimonials
        ]);
    }
    
    
    public function hireMe() {
        return view('pages.hireme');
    }

    public function aboutMe() {
        return view('pages.aboutme');
    }

    public function portfolios() {
        $portfolios = Portfolio::with('categories')->orderBy('id' , 'DESC')->paginate(12);
        $categories = Category::with('portfolio')->orderBy('id' , 'DESC')->get();
        return view('pages.portfolio' ,[
            "portfolios" => $portfolios,
            'categories' => $categories,
        ]);
    }


     public function ctForm(Request $request) 
    {
        $formValidator = $request->validate([
            'fullname'      => 'required',
            'email'     => 'required|email',
            'contact'   => 'required|integer',
            'msg'   => 'required',
        ]);

        $name       = ucfirst($request->fullname);
        $email      = $request->email;
        $contact    = $request->contact;
        $message    = $request->msg;
        $body= [
            'name' =>  $name ,
            'email' =>  $email ,
            'contact' =>  $contact ,
            'message' =>  $message ,
        ];
        
        Mail::to('info@mfgraphics.in')->send(new ContactFormMail($body));
        
        return response()->json( [
            'status' => 200,
            'msg' => 'Thanks for contacting us! We will get back to you as soon as possible'
        ] , 200 );
    }
    
}
