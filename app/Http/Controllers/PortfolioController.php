<?php

namespace App\Http\Controllers;
use App\Category;
use App\Portfolio;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        // $portfolios = Category::with('portfolio')->orderBy('id' , 'DESC')->get();
        $portfolios = Portfolio::with('categories')->orderBy('id' , 'DESC')->get();
        // return dd($portfolios);
        return view('admin.portfolio.home' , [
            'portfolios' => $portfolios
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.portfolio.create' , [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all() , [
            'featureimg' => 'required|max:8048',
            'title' => 'required|min:3|max:20',
            'category' => 'required',
        ]);


        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        } else {
            if($file = $request->file('featureimg')) {
                $name = time().time().'.'.$file->getClientOriginalExtension();
                $targetPath = public_path('/uploads/');
                if($file->move($targetPath , $name)) {
                   
                    
                    // Storing to Database
                    $data = [
                        'title' => trim(strtolower($request->title)),
                        'description' => $request->description,
                        'photo' => $name,
                        'slug'  => Str::slug($request->title , '-'),
                        'category_id' => $request->category,
                        'highligt' => ($request->highlight == 'on') ? '1' : '0',
                    ];

                    $save = Portfolio::create($data);
                    if( $save ) {
                        return redirect()->route('portfolio.index')->with('success' , 'Portfolio Added');
                    }
                }
            }

            return back()->with('uploadErr' , 'Error Uploading Images');
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::with('categories')->find($id);
        $categories = Category::all();
        return  view('admin.portfolio.edit' ,[
            'portfolio' => $portfolio,
            'categories' => $categories,
        ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , [
            'featureimg' => 'mimes:jpeg,png,jpg,bmp,mp4,webm,3gp,mov|max:8048',
            'title' => 'required|min:3|max:20',
            'category' => 'required|'

        ]);

        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        } else {
            $portfolioSave              = Portfolio::find($id);
            $portfolioSave->title       = trim(strtolower($request->title));
            $portfolioSave->description = $request->description;
            $portfolioSave->highligt = ($request->highlight == 'on') ? '1' : '0';


            $portfolioSave->category_id = $request->category;
            $portfolioSave->slug        = Str::slug($request->title , '-');

            if($file = $request->file('featureimg')) {
                $name = time().time().'.'.$file->getClientOriginalExtension();
                $targetPath = public_path('/uploads/'); 
                $portfolioSave->photo = $name;
                $file->move($targetPath , $name);
            }  

            if( $portfolioSave->save() ) {
                return redirect()->route('portfolio.index')->with('success' , 'Portfolio Updated');
            }

            return back()->with('uploadErr' , 'Error Uploading Images');
        }        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $delte = Portfolio::find($id);
        $delte->delete();
        if($delte) {
        return redirect()->route('portfolio.index')->with('success' , 'Portfolio Deleted Successfully');
        }
        return redirect()->route('portfolio.index')->with('errdel' , 'Error Deleting. Please try again');
    }

}
