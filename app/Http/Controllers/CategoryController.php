<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use App\Category;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('id' , 'DESC')->get();
        return view('admin.category.home' , [
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checking if the same name is available
        $name = Category::where('name' , '=' ,strtolower($request->name))->first();

        if($name === null) {

            $valiate = $request->validate([
                'name' => ['required' , 'max:50' , 'min:2' , ],
            ]);
    
            if($valiate) {
                
                $data = [
                    'name' => strtolower($request->name),
                    'slug' =>  Str::slug($request->name , '-'),     
                ];
                $category = Category::create($data);
                if($category) {
                    $request->session()->flash('success' , 'Successfully Created');
                } else {
                    $request->session()->flash('error' , 'Problem saving category');
                }
            }
        } 

        if($name !== null) {
            $request->session()->flash('exists' , 'Already exists');
        }
        
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::orderBy('id' , 'DESC')->get();
        $categoryData = Category::find($id);
        return view('admin.category.home' , [
            'categories' => $categories,
            'categorydata' => $categoryData,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Category::find($id);
        return view('admin.category.edit' , [
            'editData' => $editData,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $validate = $request->validate([
            'name' => ['required' , 'max:50' , 'min:2' , ],
        ]);

        $editData = Category::find($id);

        $editData->name = strtolower($request->name);
        $editData->slug = Str::slug($request->name , '-');
        
        $save = $editData->save();

        if($save) {
            return redirect()->route('category.index')->with('categoryupdated' , 'Successfully Updated');
        } 
       
        return $request->session()->flash('errorupdating' , 'Problem updating category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id)->delete();
        return redirect()->route('category.index')->with('categorydeleted' , 'Successfully Deleted');
    }
}
