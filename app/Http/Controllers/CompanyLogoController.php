<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyLogo;
use Illuminate\Support\Facades\Validator;

class CompanyLogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logos = CompanyLogo::orderBy('id' , 'DESC')->get();
        return view('admin.logowork.home' , [
            'logos' => $logos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.logowork.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() , [
            'title' => 'required|max:100',
            'category' => 'required| in:brand,company',
            'logoimg' => 'required|mimes:jpeg,png,jpg:max:1024'
        ]);

        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        } else {
            if($file = $request->file('logoimg')) {
                $name = time().time().'.'.$file->getClientOriginalExtension();
                $targetPath = public_path('/uploads/logos/');
                if($file->move($targetPath , $name)) {
                    $data = [
                        'title' => trim(strtolower($request->title)),
                        'company_logo' => $name,
                        'category' => $request->category,
                    ];
                    $save = CompanyLogo::create($data);
                    if($save) {
                        return redirect()->route('companylogo.index')->with('success' , 'Logo added successfully to '. $request->category);
                    }
                }
                return back()->with('uploadErr' , 'Error Uploading Images');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $logo = CompanyLogo::findOrFail($id);
        return view('admin.logowork.edit' , [
            'logo' => $logo,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , [
            'title' => 'required|max:100',
            'category' => 'required| in:brand,company',
        ]);

        $editData = CompanyLogo::find($id);

        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        } else {


            $editData->title = trim(strtolower($request->title));
            $editData->category = $request->category;
            $editData->company_logo = $editData->company_logo;

            if($request->hasFile('logoimg')) {
                $file = $request->file('logoimg');
                $name = time().time().'.'.$file->getClientOriginalExtension();
                $targetPath = public_path('/uploads/logos/');
                $file->move($targetPath, $name);
                $imgfile = [
                    'company_logo' => $name,
                ];
                $editData->company_logo = $name;
            }

            if($editData->save()) {
                return redirect()->route('companylogo.index')->with('success' , 'Logo updated successfully to '. $request->category);
            }
            return back()->with('uploadErr' , 'Error Uploading Images');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = CompanyLogo::find($id)->delete();

        if($delete) {
            return redirect()->route('companylogo.index')->with('success' , 'Logo Deleted Successfully');
        }
        return redirect()->route('companylogo.index')->with('errdel' , 'Error Deleting. Please try again');
    }
}
