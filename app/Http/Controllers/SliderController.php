<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Illuminate\Support\Facades\Validator;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::orderBy('id' , 'DESC')->get(); 
        return view('admin.slider.home' , [
            'sliders' => $sliders,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all() , [
            'title' => 'required',
            'img' => 'required|mimes:png,jpg,jpeg|max:2024',
            'sequence' => 'required|numeric'
        ]);

        

        if($validate->fails()) {
            return back()->withErrors($validate->errors());
        }

        $allSlider = Slider::all();
        if(count($allSlider) <= 5) {
            if($img = $request->file('img')){
                $newname = time().time().'.'.$img->getClientOriginalExtension();
    
                $targetPath = public_path('/uploads/sliders/');
    
                if($img->move($targetPath, $newname)) {
                    $data = [
                        'title' => trim($request->title) , 
                        'img'=>$newname , 
                        'sequence'=>$request->sequence
                    ];
                    $newSlide = Slider::create($data);
                    if($newSlide) {
                        return redirect()->route('slider.index')->with('success' , 'Slider Created');
                    }
                    return back()->with('error' , 'Something went wrong');
    
                }
            }
        } else {
            return back()->with('error' , 'You cannot add more than 5 Slider');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit' ,[
            'slider' => $slider,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all() , [
            'title' => 'required',
            'img' => 'mimes:png,jpg,jpeg|max:2024'
        ]);

        if($validate->fails()) {
            return back()->withErrors($validate->errors());
        }

        $updateSlider = Slider::find($id);

        $updateSlider->title = $request->title;
        $updateSlider->sequence = $request->sequence;
        
        if($img = $request->file('img')){
            $newname = time().time().'.'.$img->getClientOriginalExtension();
            $targetPath = public_path('/uploads/sliders/');
            if($img->move($targetPath, $newname)) {
                $updateSlider->img = $newname;
            } else {
                return back()->with('error' , 'Something went wrong');
            }
        }

        if($updateSlider->save()) {
            return redirect()->route('slider.index')->with('success' , 'Slider Updated');
        }
        return back()->with('error' , 'Something went wrong');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delSlider = Slider::find($id)->delete();
        return redirect()->route('slider.index')->with('success' , 'Successfully Deleted');
    }
}
