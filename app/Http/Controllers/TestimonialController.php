<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::orderBy('id' , 'DESC')->paginate(10);
        return view('admin.testimonial.home' , ['testimonials' => $testimonials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|min:3|max:100',
            'designation'=> 'required',
            'desc' => 'required',
            'image' => 'required|file|image|mimes:png,jpg,jpeg'
        ]);

        if($validation->fails()){
            return back()->withErrors($validation->errors());
        }

        $data =  [
            'title' => $request->name,
            'designation'=> $request->designation,
            'testimonialdetail' => $request->desc,
            'slug' => $request->name
        ];

        if($request->hasFile('image')){
            $image = $request->file('image');
            $newname = 'testimonial-'.time().time().'.'.$image->getClientOriginalExtension();
            $imageupload = $image->move(public_path().'/uploads/testimonial/' , $newname);
            if($imageupload) {
                $data = array_merge($data , ['image' => '/uploads/testimonial/'.$newname]);
            }  
        }

        $save = Testimonial::create($data);
        if($save) {
            return redirect()->route('testimonial.index')->with('success' ,'Testimonial added successfully');
        }
        return back()->with('error' , 'Something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::where('id' , $id)->first();
        if($testimonial ) {
            return view('admin.testimonial.edit' , [
                'testimonial' => $testimonial
            ]);
        }
        return redirect()->route('testimonial.index');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($testimon = Testimonial::find($id)) {

            $validation = Validator::make($request->all(), [
                'name' => 'required|min:3|max:100',
                'designation'=> 'required',
                'desc' => 'required',
                'image' => 'file|image|mimes:png,jpg,jpeg'
            ]);
    
            if($validation->fails()){
                return back()->withErrors($validation->errors());
            }

            $testimon->title = $request->name;
            $testimon->designation = $request->designation;
            $testimon->testimonialdetail = $request->desc;
            $testimon->slug = $request->name;
    
            if($request->hasFile('image')){
                $image = $request->file('image');
                $newname = 'testimonial-'.time().time().'.'.$image->getClientOriginalExtension();
                $imageupload = $image->move(public_path().'/uploads/testimonial/' , $newname);
                if($imageupload) {
                    $testimon->image = '/uploads/testimonial/'.$newname;
                }  
            }

            if($testimon->save()) {
                return redirect()->route('testimonial.index')->with('success' ,'Testimonial updated successfully');
            }
        }
        return back()->with('error' , 'Something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testi = Testimonial::where('id' , $id)->first();
        if($testi){
            if($testi->delete()){
                return redirect()->route('testimonial.index')->with('success' , 'Testimonial deleted successfully.');
            } 
            return back()->with('error' , 'Error deleting role');
        } 
        return back()->with('error' , 'Something went wrong');
    }
}
