<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyLogo extends Model
{
    protected $fillable = ['title' , 'category' , 'company_logo'];
}
