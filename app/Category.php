<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Portfolio;
class Category extends Model
{
    protected $fillable = ['name' , 'slug'];


    public function portfolio() {
        return $this->hasMany('App\Portfolio' , 'category_id' , 'id');
    }
}
